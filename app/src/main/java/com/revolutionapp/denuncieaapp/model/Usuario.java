package com.revolutionapp.denuncieaapp.model;

import com.revolutionapp.denuncieaapp.enums.TipoUsuario;

import java.io.Serializable;
import java.util.List;

/***
 *
 * Created by Márcio Barbosa - email: marciobarbosamobile@gmail.com
 * 04/06/2019
 *
 * */

public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String nome;
    private String instagran;
    private TipoUsuario tipo;
    private List<Denuncia> denuncias;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getInstagran() {
        return instagran;
    }

    public void setInstagran(String instagran) {
        this.instagran = instagran;
    }

    public TipoUsuario getTipo() {
        return tipo;
    }

    public void setTipo(TipoUsuario tipo) {
        this.tipo = tipo;
    }

    public void setDenuncias(List<Denuncia> denuncias) {
        this.denuncias = denuncias;
    }

    public List<Denuncia> getDenuncias() {
        return denuncias;
    }
}
