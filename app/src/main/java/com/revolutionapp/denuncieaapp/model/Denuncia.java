package com.revolutionapp.denuncieaapp.model;

import java.io.Serializable;
import java.util.List;


/***
 *
 * Created by Márcio Barbosa - email: marciobarbosamobile@gmail.com
 * 04/06/2019
 *
 * */
public class Denuncia implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String titulo;
    private String descricao;
    private String localizacao;
    private List<Usuario> envolvidos;
    private List<Imagens> imagens;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public List<Usuario> getEnvolvidos() {
        return envolvidos;
    }

    public void setEnvolvidos(List<Usuario> envolvidos) {
        this.envolvidos = envolvidos;
    }

    public List<Imagens> getImagens() {
        return imagens;
    }

    public void setImagens(List<Imagens> imagens) {
        this.imagens = imagens;
    }


    @Override
    public String toString() {
        return "Denuncia{" +
                "titulo='" + titulo + '\'' +
                '}';
    }
}
