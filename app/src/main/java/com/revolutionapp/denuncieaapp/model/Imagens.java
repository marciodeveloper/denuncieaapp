package com.revolutionapp.denuncieaapp.model;

import java.io.Serializable;


/***
 *
 * Created by Márcio Barbosa - email: marciobarbosamobile@gmail.com
 * 04/06/2019
 *
 * */

public class Imagens implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String path;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
