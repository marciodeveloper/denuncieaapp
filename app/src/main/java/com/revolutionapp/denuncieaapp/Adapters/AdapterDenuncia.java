package com.revolutionapp.denuncieaapp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.revolutionapp.denuncieaapp.R;
import com.revolutionapp.denuncieaapp.ViewHolders.ViewHolderDenuncia;
import com.revolutionapp.denuncieaapp.enums.TipoUsuario;
import com.revolutionapp.denuncieaapp.model.Denuncia;
import com.revolutionapp.denuncieaapp.model.Usuario;

import java.util.List;

public class AdapterDenuncia extends RecyclerView.Adapter<ViewHolderDenuncia> {

    private Context context;
    private List<Denuncia> denuncias;


    public AdapterDenuncia(Context context, List<Denuncia> denuncias) {
        this.context = context;
        this.denuncias = denuncias;
    }

    @NonNull
    @Override
    public ViewHolderDenuncia onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.item_list, null);
        ViewHolderDenuncia viewHolderDenuncia = new ViewHolderDenuncia(view);
        return viewHolderDenuncia;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderDenuncia viewHolderDenuncia, int i) {
        Denuncia p = denuncias.get(i);
        viewHolderDenuncia.getEdtTituloID().setText(p.getTitulo());
        for (Usuario u :p.getEnvolvidos()) {
            if(u.getTipo().equals(TipoUsuario.valueOf("ORGAO"))){
                viewHolderDenuncia.getEdtOrgaoID().setText(u.getNome());
            }

        }
    }

    @Override
    public int getItemCount() {
        return this.denuncias.size();
    }
}
