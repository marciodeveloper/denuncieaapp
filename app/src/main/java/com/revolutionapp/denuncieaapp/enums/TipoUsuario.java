package com.revolutionapp.denuncieaapp.enums;


/***
 *
 * Created by Márcio Barbosa - email: marciobarbosamobile@gmail.com
 * 04/06/2019
 *
 * */

public enum TipoUsuario {

    CIDADAO(1),
    ORGAO(2);

    TipoUsuario(int codigo) {
        this.codigo = codigo;
    }

    private final int codigo;

    public int getCodigo() {
        return this.codigo;
    }
}
