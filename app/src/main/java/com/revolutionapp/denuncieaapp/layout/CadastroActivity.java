package com.revolutionapp.denuncieaapp.layout;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.revolutionapp.denuncieaapp.Adapters.AdapterDenuncia;
import com.revolutionapp.denuncieaapp.BuildConfig;
import com.revolutionapp.denuncieaapp.R;
import com.revolutionapp.denuncieaapp.enums.TipoUsuario;
import com.revolutionapp.denuncieaapp.model.Denuncia;
import com.revolutionapp.denuncieaapp.model.Imagens;
import com.revolutionapp.denuncieaapp.model.Usuario;
import com.revolutionapp.denuncieaapp.service.RetrofitCallback;
import com.revolutionapp.denuncieaapp.service.RetrofitConfig;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CadastroActivity extends AppCompatActivity implements View.OnClickListener {

    private List<Denuncia> denuncias;
    private Button btnTirarFotoID;
    private Button btnSalvarID;
    private ImageView imageViewFotoID;
    private EditText txtTituloID;
    private EditText txtDescricaoID;
    private EditText txtOrgaoID;
    private EditText txtCidadaoID;
    private Uri uri;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private byte[] img;
    private Denuncia denunciaEdt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFFD6969")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        this.denunciaEdt = (Denuncia) intent.getSerializableExtra("edt");

        FirebaseApp.initializeApp(this);
        this.storage = FirebaseStorage.getInstance();
        this.storageReference = this.storage.getReference();

        this.imageViewFotoID = findViewById(R.id.imageViewFotoID);
        this.denuncias = new ArrayList<>();
        this.btnTirarFotoID = findViewById(R.id.btnTirarFotoID);
        this.btnTirarFotoID.setOnClickListener(this);
        this.btnSalvarID = findViewById(R.id.btnSalvarID);
        this.btnSalvarID.setOnClickListener(this);
        this.txtTituloID = findViewById(R.id.txtTituloID);
        this.txtDescricaoID = findViewById(R.id.txtDescricaoID);
        this.txtOrgaoID = findViewById(R.id.txtOrgaoID);
        this.txtCidadaoID = findViewById(R.id.txtCidadaoID);

        if(this.denunciaEdt != null){
            this.txtTituloID.setText(this.denunciaEdt.getTitulo());
            this.txtDescricaoID.setText(this.denunciaEdt.getDescricao());
            this.txtOrgaoID.setText(this.denunciaEdt.getEnvolvidos().get(1).getInstagran());
            this.txtCidadaoID.setText(this.denunciaEdt.getEnvolvidos().get(0).getInstagran());
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(data != null){
            Bundle bundle = data.getExtras();
            if(bundle != null){
                Bitmap img = (Bitmap) bundle.get("data");
                Intent novaIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, this.uri);
                sendBroadcast(novaIntent);

                String caminhoDaImagem = uri.getPath();
                Bitmap mImageBitmap = null;
                try {
                     mImageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(caminhoDaImagem));
                     ByteArrayOutputStream out = new ByteArrayOutputStream();
                     mImageBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                     this.img = out.toByteArray();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                this.imageViewFotoID.setImageBitmap(mImageBitmap);
            }
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if(id == R.id.btnTirarFotoID){

            if ( ContextCompat.checkSelfPermission( this, android.Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
                ActivityCompat.requestPermissions( this, new String[] {  android.Manifest.permission.CAMERA  },0);
            }

            File diretorio = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File imagem = new File(diretorio.getPath() + "/" + System.currentTimeMillis() + ".jpg");
            this.uri = FileProvider.getUriForFile(CadastroActivity.this, BuildConfig.APPLICATION_ID + ".provider",imagem);

            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            intent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            startActivityForResult(intent, 0);

        }else if(id == R.id.btnSalvarID && this.denunciaEdt == null){
            Denuncia denuncia = new Denuncia();
            List<Usuario> usuarios = new ArrayList<>();
            Usuario  user = new Usuario();
            user.setTipo(TipoUsuario.CIDADAO);
            user.setNome(this.txtCidadaoID.getText().toString());
            user.setInstagran(this.txtCidadaoID.getText().toString());

            Usuario  user2 = new Usuario();
            user2.setTipo(TipoUsuario.ORGAO);
            user2.setNome(this.txtOrgaoID.getText().toString());
            user2.setInstagran(this.txtOrgaoID.getText().toString());

            usuarios.add(user);
            usuarios.add(user2);

            denuncia.setEnvolvidos(usuarios);
            denuncia.setLocalizacao("TESTE MOCK");

            Imagens imagem = new Imagens();
            imagem.setPath(this.imageViewFotoID.toString());

            denuncia.setImagens(Arrays.asList(imagem));
            denuncia.setDescricao(this.txtDescricaoID.getText().toString());
            denuncia.setTitulo(this.txtTituloID.getText().toString());

            Call<Denuncia> callCadastro =  new RetrofitConfig().denunciaService().addDenuncia(denuncia);
            callCadastro.enqueue(new Callback<Denuncia>(){

                @Override
                public void onResponse(Call<Denuncia> call, Response<Denuncia> response) {
                    if(response.isSuccessful()) {
                        StorageReference ref = storage.getReference(uri.getPath());
                        StorageMetadata metadata = new StorageMetadata.Builder().setCustomMetadata("img", "img-dnuncia").build();
                        UploadTask up = ref.putBytes(img, metadata);
                        Intent intent = new Intent(CadastroActivity.this, ExibirDenunciasActivity.class);
                        intent.putExtra("", response.body());
                        setResult(RESULT_OK,intent);
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<Denuncia> call, Throwable t) {
                    Log.e("Error", t.getMessage());
                }
            });

        }else if(id == R.id.btnSalvarID && this.denunciaEdt != null){
            Denuncia denuncia = denunciaEdt;
            List<Usuario> usuarios = denunciaEdt.getEnvolvidos();

            denunciaEdt.getEnvolvidos().get(0).setNome(this.txtCidadaoID.getText().toString());
            denunciaEdt.getEnvolvidos().get(0).setInstagran(this.txtCidadaoID.getText().toString());

            denunciaEdt.getEnvolvidos().get(1).setNome(this.txtOrgaoID.getText().toString());
            denunciaEdt.getEnvolvidos().get(1).setInstagran(this.txtOrgaoID.getText().toString());

            denunciaEdt.setLocalizacao("TESTE MOCK ALTERADO");

            Imagens imagem = new Imagens();
            imagem.setPath(this.imageViewFotoID.toString());
            denunciaEdt.setImagens(Arrays.asList(imagem));
            denunciaEdt.setDescricao(this.txtDescricaoID.getText().toString());
            denunciaEdt.setTitulo(this.txtTituloID.getText().toString());


            Call<Denuncia> denunciaEditada =  new RetrofitConfig().denunciaService().edtDenuncia(denunciaEdt);
            denunciaEditada.enqueue(new Callback<Denuncia>(){

                @Override
                public void onResponse(Call<Denuncia> call, Response<Denuncia> response) {
                    if(response.isSuccessful()) {
                        Intent intent = new Intent(CadastroActivity.this, ExibirDenunciasActivity.class);
                        intent.putExtra("", response.body());
                        setResult(RESULT_OK,intent);
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<Denuncia> call, Throwable t) {
                    Log.e("Error", t.getMessage());
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
