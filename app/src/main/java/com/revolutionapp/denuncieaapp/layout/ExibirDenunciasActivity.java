package com.revolutionapp.denuncieaapp.layout;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.revolutionapp.denuncieaapp.Adapters.AdapterDenuncia;
import com.revolutionapp.denuncieaapp.Adapters.ClickListener;
import com.revolutionapp.denuncieaapp.Adapters.RecyclerTouchListener;
import com.revolutionapp.denuncieaapp.R;
import com.revolutionapp.denuncieaapp.model.Denuncia;
import com.revolutionapp.denuncieaapp.service.RetrofitCallback;
import com.revolutionapp.denuncieaapp.service.RetrofitConfig;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;

public class ExibirDenunciasActivity extends AppCompatActivity {

    private List<Denuncia> denuncias = new ArrayList<>();
    private RecyclerView rcvDenuncias;
    private AdapterDenuncia adapterDenuncia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exibir_denuncias);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFFD6969")));

        this.rcvDenuncias = findViewById(R.id.rcvDenunciasID);
        this.rcvDenuncias.setLayoutManager(new LinearLayoutManager(this));

        this.listarDenuncias();
    }

    public void listarDenuncias(){
        Call<List<Denuncia>> call = new RetrofitConfig().denunciaService().listar();

        call.enqueue(new RetrofitCallback<List<Denuncia>>(this) {
            @Override
            public void onSuccess(List<Denuncia> denunciasCall) {
                denuncias = denunciasCall;
                adapterDenuncia = new AdapterDenuncia(getApplicationContext(), denuncias);
                rcvDenuncias.setAdapter(adapterDenuncia);
            }
        });

        rcvDenuncias.addOnItemTouchListener(new RecyclerTouchListener(this, rcvDenuncias, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent i = new Intent(ExibirDenunciasActivity.this, CadastroActivity.class);
                Denuncia d = denuncias.get(position);
                i.putExtra("edt",d);
                startActivityForResult(i, RESULT_OK);
            }

            @Override
            public void onLongClick(View view, int position) {

                Denuncia d = denuncias.get(position);

                Call<ResponseBody> call = new RetrofitConfig().denunciaService().deleteDenuncia(d.getId().intValue());
                call.enqueue(new RetrofitCallback<ResponseBody>(getApplicationContext()) {
                    @Override
                    public void onSuccess(ResponseBody arg0) {
                        Toast.makeText(ExibirDenunciasActivity.this, "Denuncia Excluida com sucesso:",Toast.LENGTH_SHORT).show();
                        listarDenuncias();
                    }
                });

            }
        }));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_denuncia_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.itemAddID:
                Intent i = new Intent(ExibirDenunciasActivity.this, CadastroActivity.class);
                startActivityForResult(i, 1);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            Denuncia denuncia = (Denuncia)data.getExtras().getSerializable("response");
            this.denuncias.add(denuncia);
            adapterDenuncia = new AdapterDenuncia(getApplicationContext(), denuncias);
            rcvDenuncias.setAdapter(adapterDenuncia);
        }
    }
}
