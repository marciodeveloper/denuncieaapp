package com.revolutionapp.denuncieaapp.service;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.revolutionapp.denuncieaapp.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class RetrofitCallback<T> implements Callback<T> {

    private Context context;

    public RetrofitCallback(Context context) {
        this.context = context;
    }

    public abstract void onSuccess(T arg0);


    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful() && response.code() == 200) {
            onSuccess(response.body());
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        Log.e(context.getString(R.string.Error_integracao), t.getMessage());
        Toast.makeText(context, context.getString(R.string.descricao_error_integracao), Toast.LENGTH_SHORT).show();
    }
}
