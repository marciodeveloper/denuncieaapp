package com.revolutionapp.denuncieaapp.service;

import com.revolutionapp.denuncieaapp.model.Denuncia;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface DenunciaService {


    @GET("denuncia")
    Call<List<Denuncia>> listar();

    @POST("denuncia")
    Call<Denuncia> addDenuncia(@Body Denuncia d);

    @PUT("denuncia/alterar")
    Call<Denuncia> edtDenuncia(@Body Denuncia d);

    @DELETE("denuncia/deletar/{id}")
    Call<ResponseBody> deleteDenuncia(@Path("id") int id);
}
