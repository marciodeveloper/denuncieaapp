package com.revolutionapp.denuncieaapp.service;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class RetrofitConfig {


    private Retrofit retrofit;

    public RetrofitConfig(){
        this.retrofit = new Retrofit.Builder().baseUrl("http://192.168.43.82:8080/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
    }

    public DenunciaService denunciaService(){
        return this.retrofit.create(DenunciaService.class);
    }

}
