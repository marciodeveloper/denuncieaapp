package com.revolutionapp.denuncieaapp.ViewHolders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.revolutionapp.denuncieaapp.R;

public class ViewHolderDenuncia extends RecyclerView.ViewHolder {

    private ImageView imageDenunciaID;
    private TextView edtTituloID, edtOrgaoID;

    public ViewHolderDenuncia(@NonNull View itemView) {
        super(itemView);
        this.imageDenunciaID = itemView.findViewById(R.id.imageDenunciaID);
        this.edtTituloID = itemView.findViewById(R.id.edtTituloID);
        this.edtOrgaoID = itemView.findViewById(R.id.edtOrgaoID);
    }

    public ImageView getImageDenunciaID() {
        return imageDenunciaID;
    }

    public void setImageDenunciaID(ImageView imageDenunciaID) {
        this.imageDenunciaID = imageDenunciaID;
    }

    public TextView getEdtTituloID() {
        return edtTituloID;
    }

    public void setEdtTituloID(TextView edtTituloID) {
        this.edtTituloID = edtTituloID;
    }

    public TextView getEdtOrgaoID() {
        return edtOrgaoID;
    }

    public void setEdtOrgaoID(TextView edtOrgaoID) {
        this.edtOrgaoID = edtOrgaoID;
    }
}
